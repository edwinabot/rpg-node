const Personaje = require('../models/Personaje');

function PersonajesController() {

}

PersonajesController.prototype.constructor = PersonajesController;

PersonajesController.prototype.get = function (req, res, next) {
    res.render('personajes/form_personaje');
};

PersonajesController.prototype.post = function (req, res, next) {
    return Personaje.prototype.crearPersonaje(req.body).then(
        function (data) {
            res.send("<h1>"+ data + "</h1>");
        }, 
        function (data) {
            res.render('personajes/form_personaje', data);
        }
    );
};

module.exports = PersonajesController;