var db = require('../config/database');
/*
 * Voy a hacer modelos ActiveRecord eager loading
 */
function Personaje(nombre, clase, alineamiento) {
    this.nombre = nombre;
    this.clase = clase;
    this.alineamiento = alineamiento;
}

Personaje.prototype.constructor = Personaje;

Personaje.prototype.validar = function (data) {
    return new Promise(function (resolve, reject) {
        let valido = true;
        valido = data.nombre !== "" && data.clase !== "" && data.alineamiento !== "";
        valido ? resolve(data) : reject(data, "Faltan datos");
    });
};

Personaje.prototype.crearPersonaje = function (data) {
    return new Promise(function (resolve, reject) {
        Personaje.prototype.validar(data)
        .then(Personaje.prototype.save)
        .then(resolve)
        .catch(reject);
    });
};

Personaje.prototype.save = function (data) {
    return new Promise(function (resolve, reject) {
        var personaje  = {nombre: data.nombre, clase: data.clase, alineamiento: data.alineamiento};
        var query = db.query('INSERT INTO personajes SET ?', personaje, function (error, results, fields) {
            if (error) throw error;
            console.log(query.sql);
            resolve(results.insertId);
        });
    });
};

module.exports = Personaje;