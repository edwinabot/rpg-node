var express = require('express');
var PersonajesController = require('../controllers/PersonajesController');
var router = express.Router();

/* GET users listing. 
router.get('/', function(req, res, next) {
  res.render('personajes/form_personaje');
});

router.post('/', function(req, res, next) {
    console.log(req.body);
    PersonajesController.crearPersonaje(req.body);
    res.render('personajes/form_personaje', req.body);
});
*/
router.get('/', PersonajesController.prototype.get);
router.post('/', PersonajesController.prototype.post);

module.exports = router;